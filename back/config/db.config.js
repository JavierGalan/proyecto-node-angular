const mongoose = require('mongoose');
const DB_URL = "mongodb://localhost:27017/proyecto-node-angular";

const connect = async () => {
    try {
        await mongoose.connect(DB_URL, {
          useUnifiedTopology: true,
          useNewUrlParser: true,
        });
        console.log('DB connected successfully');
    } catch (error) {
        console.log("Error trying to connect to DB");
        console.log(error);
    }
};

module.exports = {DB_URL, connect};