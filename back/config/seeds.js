const mongoose = require("mongoose");
const User = require("../models/User.model");
const db = require("./db.config");

const users = [
  {
    userName: "User1",
    password: "password1",
    email: "email@email.com",
  },
  {
    userName: "User2",
    password: "password2",
    email: "email2@email.com",
  },
  {
    password: "password3",
    email: "email3@email.com",
  },
];

mongoose
  .connect(db.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(async () => {
    const allUsers = await User.find();
    if (allUsers.length) {
      console.log(`[Find]: ${allUsers.length} users`);
      await User.collection.drop();
      console.log("[Delete]: Colection erased");
    } else {
      console.log("[Find]: Users not found");
    }
  })
  .catch((error) => console.log("[Error]: Dropping collection -->", error))
  .then(async () => {
    await User.insertMany(users);
    console.log("[Success]: Adding new users");
  })
  .catch((error) => console.log("[Error]: Adding new users -->", error))
  .finally(()=> mongoose.disconnect());