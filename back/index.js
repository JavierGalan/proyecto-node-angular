const express = require('express');

require('./config/db.config');
const db = require("./config/db.config");
db.connect();

const PORT = 3000;
const app = express();

// rutas

const indexRoutes = require('./routes/index.routes');
const userRoutes = require('./routes/user.routes');

app.use("/", indexRoutes);
app.use('/users', userRoutes);
app.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
});

app.use((err, req, res, next) => {
    return res.status(err.status || 500).json(err.message || 'Unexpected error');
});

app.listen(PORT, ()=>{
    console.log(`Server running in http:/localhost${PORT}`);
});