const mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

const { Schema } = mongoose;

const groupSchema = new Schema(
  {
    name: { type: String, required: true },
    todos: [{ type: mongoose.Types.ObjectId, ref: "Todo" }],
    users: [{ type: mongoose.Types.ObjectId, ref: "User" }],
  },
  { timestamps: true }
);

const Group = mongoose.model("Group", groupSchema);

module.exports = Group;
