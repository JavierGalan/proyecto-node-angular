const mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

const { Schema } = mongoose;

const todoSchema = new Schema(
  {
    title: { type: String, required: true },
    tasks: [{ type: String }],
  },
  { timestamps: true }
);

const Todo = mongoose.model("Todo", todoSchema);

module.exports = Todo;
