const express = require('express');

const User = require('../models/User.model');

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const users = await User.find();
        return res.status(200).json(users);
    } catch (error) {
        return res.status(500).json(error);
    }
});

module.exports = router;