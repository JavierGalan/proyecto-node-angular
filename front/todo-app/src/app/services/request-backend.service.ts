import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RequestBackendService {
  constructor(private serverRequest: HttpClient) {}
  public getData(part:string){
    return this.serverRequest.get(`${environment.baseUrl}/${part}`);
  }
}
